import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';

import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import { withRouter } from 'react-router-dom'
import config from '../../config'
import Image from './123.jpg'
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: '15vh'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginBottom: 20,
    //minHeight: '30vh',
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
  },
  deleteIcon: {
    backgroundColor: '#f02043'
  },
  paperContainer: {
    // background: `url(${Image}) 100% 100% no-repeat`,
    backgroundSize: 'cover',
    minHeight: '100%',

  },
  productInfo: {
    Height: '30vh',
  },
  img: {
    Height: '100vh',
    width: '100vh',
    borderRadius: '1vh',
  },
  productsHeader: {
    marginBottom: '20px',
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },

}));


function RoomBody(props) {
  const classes = useStyles();

  const queryParamsString = props.location.search.substring(1), // remove the "?" at the start
    searchParams = new URLSearchParams(queryParamsString);
  let id = searchParams.get("id");

  console.log(id)

  const [productInfo, setData] = useState({
    "id": "int",
    "name": "str",
    "price": "int",
    "category": "str",
    "category_id": "int",
    "description": "str",
    "photo_url": "str"
  });

  let isFull = false;

  useEffect(() => {

    axios.get(config.server_ip + '/rooms/1', {
      params: {
      }
    })
      .then(function (response) {

        console.log(response)
        if (response.data.max_product = response.data.product_count)
          isFull = true;
      })
      .catch(function (error) {
        console.log(error);
      });


    axios.get(config.server_ip + '/products/' + id, {
    })
      .then(function (response) {
        setData(response.data);
        console.log(response.data)
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);


  const [open, setOpen] = React.useState(false);

  const [messageText, setText] = React.useState("Предмет успешно добавлен в комнату!");

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  //let messageText = "Предмет успешно добавлен в комнату!"



  const buyItem = (id) => {

    if (isFull === true) {
      // messageText = "Вы не можете добавить больше предметов в комнату :("
      setText("Вы не можете добавить больше предметов в комнату :(")
      console.log(messageText)
      handleClickOpen()
    }
    else {

      console.log(id)
      axios.post(config.server_ip + '/products/buy', {
        "product_id": id,
        "room_id": 1,
        "user_id": 1
      })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
      handleClickOpen()

      // props.history.push('/room')
    }

  }

  return (

    <div className={classes.root}>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <br /><br />
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {messageText}
        </DialogTitle>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Закрыть
      </Button>
        </DialogActions>
      </Dialog>

      <Container maxWidth="lg" component="main" className={classes.heroContent}>
        <Grid container spacing={10}>
          <Grid item xs={5}>
            <Card className={classes.productInfo}>
              <CardHeader
                title={productInfo.name}
                titleTypographyProps={{ align: 'center' }}
                subheaderTypographyProps={{ align: 'center' }}
                className={classes.cardHeader}
              />
              <CardContent>
                <div className={classes.cardPricing}>
                  <Typography component="h2" variant="h4" color="textPrimary">
                    ${productInfo.price}
                  </Typography>
                </div>
                <Typography align="center">
                  {productInfo.description}
                </Typography>
              </CardContent>
              <CardActions>
                <Button fullWidth variant='contained' onClick={() => buyItem(id)} color="primary">
                  {/* <Link variant="button" color="textPrimary"  className={classes.link}> */}
                  Купить
                  {/* </Link> */}
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={7}>
            <img className={classes.img} src={productInfo.photo_url}></img>
          </Grid>
        </Grid>
      </Container >

    </div >
  );
}

export default withRouter(RoomBody)