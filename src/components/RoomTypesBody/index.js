import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import config from '../../config.json'
import { withRouter } from 'react-router-dom'


const useStyles = makeStyles(theme => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(12, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
}));

function Pricing(props) {
  const classes = useStyles();

  const [roomTypes, setData] = useState([]);

  axios.get(config.server_ip + '/users/1/first_enter', {
  })
    .then(function (response) {
      if (response.data.status == "NO"){
        props.history.push('/room')
      }
        
      console.log(response)
    })
    .catch(function (error) {
      console.log(error);
    });

  const setRoomType = (roomTypeId) => {
    axios.post(config.server_ip + '/room_types', {
      "room_type_id": roomTypeId,
      "user_id": 1,
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  useEffect(() => {

    axios.get(config.server_ip + '/room_types', {
      params: {
      }
    })
      .then(function (response) {
        console.log(response.data)
        let arrResponse = [];
        arrResponse = response.data.room_type;
        console.log("**", arrResponse)
        setData(arrResponse);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  console.log("***", roomTypes)

  return (
    <React.Fragment>
      <CssBaseline />
      {/* Hero unit */}
      <Container maxWidth="sm2" component="main" className={classes.heroContent}>
        <Typography component="h1" variant="h3" align="center" color="textPrimary" gutterBottom>
          Добро пожаловать!
        </Typography>
        <Typography variant="h5" align="center" color="textSecondary" component="p">
          Выберите тип комнаты. Есть варианты с различной площадью и возможным количеством мебели.
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {roomTypes.map(tier => (
            // Enterprise card is full width at sm breakpoint
            <Grid item xs={12} md={4}>
              <Card>
                <CardHeader
                  title={tier.name}
                  titleTypographyProps={{ align: 'center' }}
                  subheaderTypographyProps={{ align: 'center' }}
                  className={classes.cardHeader}
                />
                <CardContent>
                  <div className={classes.cardPricing}>
                    <Typography component="h2" variant="h3" color="textPrimary">
                      ${tier.price}
                    </Typography>
                    <Typography variant="h6" color="textSecondary">
                    </Typography>
                  </div>
                  <ul>
                    <Typography component="li" variant="subtitle1" align="Left">
                      ✔ Площадь: {tier.area} м<sup>2</sup>
                    </Typography>
                    <Typography component="li" variant="subtitle1" align="Left">
                      ✔ До {tier.max_products} предметов мебели
                    </Typography>
                  </ul>
                </CardContent>
                <CardActions>
                  <Button fullWidth variant='contained' onClick={() => setRoomType(tier.id)} href='room' color="primary">
                    {/* <Link variant="button" color="textPrimary"  className={classes.link}> */}
                    Выбрать
                    {/* </Link> */}
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>

    </React.Fragment>
  );
}

export default withRouter(Pricing)
