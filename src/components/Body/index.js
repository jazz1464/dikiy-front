
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';



const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 345,
    },

    media: {
    height: 140,
  },

}));

export default function ImgMediaCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root} style={{marginTop: '50px'}}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
        //   image="https://images.app.goo.gl/tFn2XZbsc1pfTCTD6"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Например приставка
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Какая-то инфа о товаре
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          Купить
        </Button>
      </CardActions>
    </Card>
  );
}
