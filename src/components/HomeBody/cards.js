import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import main1 from '../../assets/main1.jpg';
import main2 from '../../assets/main2.jpg';
import main3 from '../../assets/main3.jpg';

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  additionalInfo: {
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
  },
  heroContent: {
    // backgroundColor:
    //   theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
    padding: theme.spacing(8, 0, 0),

  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  getAppText: {
    marginRight: '3rem',
    marginTop: '1.5rem',
    marginBottom: '3rem',
    fontSize: '2em'
  },
  getApp: {
    marginTop: '3.5rem',
    fontSize: '1.5em',
    // marginBottom: '1.5rem',
  },
}));

const cards = [
  {
    number: 1,
    title: 'Прокачай комнату!',
    description: 'Апгрейдь свою комнату как хочешь: дизайнерская мебель, современная техника и всё такое!',
    photo: main1,
  },
  {
    number: 2,
    title: 'Развлечения',
    description: 'Заказывай любые (почти) развлечения прямо в комнату. Игровые приставки, аудиосистем и музыкальные инструменты.',
    photo: main2,
  },
  {
    number: 3,
    title: 'Шикарный вид',
    description: 'Общежитие находится в центре города с шикарным видом на ночной мегаполис.',
    photo: main3,
  },
];

// const cards = [1, 2, 3];

export default function Album() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        <div className={classes.additionalInfo}>
          {/* Hero unit */}
          <div className={classes.heroContent}>
            <Container maxWidth="sm">
              <Typography component="h1" variant="h2" align="center" color="textPrimary" paragraph>
                У нас круто
            </Typography>
            </Container>
          </div>
          <Container className={classes.cardGrid} maxWidth="md">
            {/* End hero unit */}
            <Grid container spacing={4}>
              {cards.map(card => (
                <Grid item key={card} xs={12} sm={6} md={4}>
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image = {card.photo}
                      title="Рум апгрейд"
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2" align="center">
                        {card.title}
                      </Typography>
                      <Typography align="center">
                        {card.description}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        </div>
        <Divider variant="middle" paddingBottom="50px" />
        <div>
          <Container maxWidth="100%">
            <div className={classes.heroButtons}>
              <Grid container justify="flex-end" >
                <Grid item xs={4}>
                  <Typography className={classes.getAppText} align="left" color="textSecondary" paragraph>
                    Все функции доступны в удобном и бесплатном мобильном приложении, попробуй!
            </Typography>
                </Grid>
                <Grid item xs={5}>
                  <Button className={classes.getApp} variant="contained" color="primary">
                    Скачать в PlayMarket
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        <Divider variant="middle" paddingBottom="50px" />

      </main>
    </React.Fragment>
  );
}