import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import logo from '../../assets/untitled.svg'


const useStyles = makeStyles(theme => ({
  mainFeaturedPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    //marginBottom: theme.spacing(0),
    backgroundImage: '../../assets/HomeBG.png',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(23,55,94,.7)',
  },
  mainFeaturedPostContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    }
    
  },
    logotext: {
        fontSize: '6rem'
    },
    text2: {
        fontSize: '2rem',
        marginBottom: '2rem'
    },
    text3: {
        fontSize: '1rem',
        marginBottom: '2rem'
    }
}));

export default function MainFeaturedPost(props) {
  const classes = useStyles();
  const { post } = props;

  return (
    <Paper className={classes.mainFeaturedPost} style={{ backgroundImage: `url(${post.image})` }}>
      {/* Increase the priority of the hero background image */}
      {<img style={{ display: 'none' }} src={post.image} alt={post.imageText} />}
      <div className={classes.overlay} />
      <Grid container>
        <Grid item md={12}>
          <div className={classes.mainFeaturedPostContent}  align = "center"> 
            <div backgroundImage="logo" size> 
            </div>
            <img src={logo} alt="Logo" height= "150px" width="200px" />
            
            <Typography className = {classes.logotext} color="inherit">
              <b>{post.title}</b>
            </Typography>
            
            <Typography className={classes.text2} color="inherit" gutterBottom >
              <b>{post.title2}</b>
            </Typography>
            
            <Typography className={classes.text3} color="inherit" paragraph>
              {post.description}
            </Typography>

            <Button size="Large" maxwidth='150px' variant='contained' href="/authorization" color="primary"  gutterBottom >
              Войти
            </Button>
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
}

MainFeaturedPost.propTypes = {
  post: PropTypes.object,
};
