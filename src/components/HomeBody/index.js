import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import MainFeaturedPost from './MainFeaturedPost.js';
import HomeBG from '../../assets/HomeBG.png';
import Album from './cards'

const useStyles = makeStyles(theme => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));

const mainFeaturedPost = {
  title: 'ВОбщаге!',
  title2: 'Общежитие премиум класса',
  description: "Лучшее студенческое общежитие 2020 года по нашей версии",
  image: HomeBG,
  imgText: 'main image description',
  linkText: 'Continue reading…',
};

export default function Blog() {
  const classes = useStyles();

  return (
    <React.Fragment>

          <MainFeaturedPost post={mainFeaturedPost} />
          <Album />

    </React.Fragment>
  );
}


