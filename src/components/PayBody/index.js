import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import axios from 'axios'
import config from '../../config'
import { withRouter } from 'react-router-dom'


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: '25vh',
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        //width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function Pay(props) {
    const classes = useStyles();

    const [paySum, setSum] = useState(0);

    let path = '/roomTypes';
    
    useEffect(() => {
        axios.get(config.server_ip + '/users/1/first_enter', {
        })
            .then(function (response) {
                if (response.data.status == "NO") {
                    path = '/room';
                }
                console.log(response)
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);

    const onSubmitForm = (event) => {

        axios.post(config.server_ip + '/users/1/wallet/charge', {
            "user_id": 1,
            "value": paySum,
        })
            .then(function (response) {
                props.history.push(path)
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <Container className={classes.root} component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Пополнение кошелька
                </Typography>
                {/* <form className={classes.form} noValidate> */}
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="email"
                            label="Номер карты"
                            name="email"
                            autoComplete="email"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            name="password"
                            label="Имя и фамилия владельца"
                            id="password"
                            autoComplete="current-password"
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            autoComplete="fname"
                            name="firstName"
                            variant="outlined"
                            required
                            fullWidth
                            id="firstName"
                            label="Срок действия"
                            autoFocus
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="lastName"
                            label="CVC/CVV"
                            name="lastName"
                        //autoComplete="lname"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="sum"
                            label="Введите сумму"
                            name="sum"
                            onChange={(event) => setSum(event.target.value)}
                        //autoComplete="lname"
                        />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={onSubmitForm}
                >
                    Пополнить
                    </Button>
                {/* </form> */}
            </div>
        </Container>
    );
}

export default withRouter(Pay)