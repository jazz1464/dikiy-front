import React, {useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import config from '../../config.json'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import logo from '../../assets/untitled.svg'

const useStyles = makeStyles(theme => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
    },
}));

export default function Header() {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    
    const [balance, setData] = useState([]);

    useEffect(() => {
            axios.get(config.server_ip + '/users/1', {
                params: {
                }
            })
            .then(function (response) {  
                console.log(response.data)
                let arrResponse = [];
                arrResponse = response.data;
                console.log("**",arrResponse)
                setData(arrResponse);
            })
            .catch(function (error) {
                console.log(error);
            });
    },[]);

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="fixed" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <a href="/"><img width="100" height="60" src={logo} alt="Logo" /></a>
                    <nav className={classes.toolbarTitle}>
                    </nav>
            </Toolbar>
            </AppBar>
        </React.Fragment>
    );
}
