
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ItemImage from './1.png'
import axios from 'axios'
import config from '../../../config' 


const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 345,
    },

    media: {
    height: 140,
  },

}));

export default function ImgMediaCard({price, name, id, count, photo_url}) {
  const classes = useStyles();

  const deleteItem = (id) => {

    console.log(id)

    axios.post(config.server_ip + '/products/delete', {
      "product_id": id,
      "room_id": 1,
      "user_id": 1
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  return (
    <Card className={classes.root} style={{marginTop: '50px'}}>
      <CardActionArea href={'/productPage?id=' + id}>
        <CardMedia
          className={classes.media}
           image={photo_url}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
           {name}
          </Typography>
          <Typography variant="h6" color="black" component="p" >
            Количество: {count}
          </Typography>
          <Typography variant="h6" color="black" component="p" >
            Возврат: ${price/2}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions style={{justifyContent: 'center'}}>
        <Button size="large" color="primary" onClick={() => deleteItem(id)} href="room" variant="outlined">
          Удалить
        </Button>
      </CardActions>
    </Card>
  );
}
