import React from 'react'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const useStyles = makeStyles(theme => ({

    root: {
        flexGrow: 1,
        marginTop: '15vh'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        marginBottom: 20,
        //minHeight: '30vh',
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
    },
    deleteIcon: {
        backgroundColor: '#f02043'
    },
    paperContainer: {
        background: `url(${Image}) 100% 100% no-repeat`,
        backgroundSize: 'cover',
        minHeight: '100%',
    },
    productsHeader: {
        marginBottom: '20px',
    }

}));

export default function Product({ name, price }) {
    const classes = useStyles();

    return (
        <div>
            <Paper className={classes.paper}>
                <Grid container spacing={12}>
                    <Grid item xs={8}>
                        <Typography color='textPrimary' variant="h6" align="left" component="h6">
                            {name}
                        </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography color='textPrimary' variant="h6" align="left" component="h6">
                            {price} $
                        </Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="h6" component="h6">
                            <DeleteForeverIcon color="primary" style={{ fontSize: 30 }} />
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    )
}
