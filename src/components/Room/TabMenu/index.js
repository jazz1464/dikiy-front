import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


const useStyles = makeStyles(theme => ({
    Tabs: {
        marginTop: '10vh',
        backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
        
    }
}));

export default function Header({ onChangeTabId }) {
    const classes = useStyles();

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        onChangeTabId(newValue)
        setValue(newValue)
    };

    console.log(value)


    return (
        <>
            <CssBaseline />
            <div>

                <Paper className={classes.Tabs}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                    >
                        <Tab label="Все категории" />
                        <Tab label="Мебель" />
                        <Tab label="Бытовая техника" />
                        <Tab label="Мультимедиа" />
                    </Tabs>
                </Paper>


            </div>
        </>
    );
}
