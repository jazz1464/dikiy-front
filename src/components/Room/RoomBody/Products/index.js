import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
//import Slideshow from '../Room';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ImgMediaCard from '../../CatalogItem';
import Box from '@material-ui/core/Box';

import axios from 'axios';
import config from '../../../../config.json'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    // maxWidth: 345,
    marginTop: '5vh',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return value === index && children
}

export default function FullWidthGrid({categoryId}) {
  const classes = useStyles();

  const [products, setProducts] = useState([]);

  useEffect(() => {
          axios.get(config.server_ip + '/rooms/1', {
          })
          .then(function (response) {  
            //console.log(response.data)
            let arrResponse = [];
            arrResponse = response.data.products;
            //arrResponse.push(response.data);
            console.log(arrResponse)
            setProducts(arrResponse);
          })
          .catch(function (error) {
            console.log(error);
          });
  },[]);


  return (
    <div className={classes.root}>
      <Container maxWidth="lg" component="main" className={classes.heroContent}>
        <Grid container spacing={3}>
          <TabPanel value={categoryId} index={0}>
          {
              products !== null && products.length !== 0 && products.map(({price, id, name, count, photo_url}) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard photo_url={photo_url} count={count} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>
          
          <TabPanel value={categoryId} index={1}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 1).map(({price, id, name, count, photo_url}) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard photo_url={photo_url} count={count} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>

          <TabPanel value={categoryId} index={2}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 2).map(({price, id, name, count, photo_url}) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard photo_url={photo_url} count={count} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
        </TabPanel>

        <TabPanel value={categoryId} index={3}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 3).map(({price, id, name, count, photo_url}) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard photo_url={photo_url} count={count} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
        </TabPanel>
      </Grid>
      </Container >
    </div>
  );
}