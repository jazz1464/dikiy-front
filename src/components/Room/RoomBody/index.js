import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import config from '../../../config.json'
import Product from '../Product';
import axios from 'axios';
import Image from './123.jpg'
import TabMenu from '../TabMenu'
import Products from './Products'

const useStyles = makeStyles(theme => ({

  root: {
    flexGrow: 1,
    marginTop: '15vh'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginBottom: 20,
    //minHeight: '30vh',
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
  },
  deleteIcon: {
    backgroundColor: '#f02043'
  },
  paperContainer: {
    // background: `url(${Image}) 100% 100% no-repeat`,
    backgroundSize: 'cover',
    minHeight: '100%',
  },
  productsHeader: {
    marginBottom: '20px',
  }

}));


export default function RoomBody() {
  const classes = useStyles();

  const [categoryId, setCategoryId] = useState(0);

  const [products, setProducts] = useState([]);
  const [roomInfo, setInfo] = useState({
    "id": "",
    "name": "",
    "room_type": "",
    "area": "",
    "max_product": "",
    "product_count": "",
  });

  useEffect(() => {
    axios.get(config.server_ip + '/rooms/1', {
      params: {
      }
    })
      .then(function (response) {
        console.log(response.data)
        let arrResponse = [];
        arrResponse = response.data.products;
        //arrResponse.push(response.data);
        setProducts(arrResponse);
        console.log(response.data)
        setInfo(response.data)
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);


  console.log(products)

  return (
    <div className={classes.root}>
      <Container maxWidth="lg" component="main" className={classes.heroContent}>
        <Grid container spacing={10}>
          <Grid item xs={4}>
            <Card>
              <CardHeader
                title="Информация"
                titleTypographyProps={{ align: 'center' }}
                subheaderTypographyProps={{ align: 'center' }}
                className={classes.cardHeader}
              />
              <CardContent>
                <Table className={classes.table} aria-label="simple table">
                  <TableBody>

                    <TableRow >
                      <TableCell component="th" scope="row">Номер команаты</TableCell>
                      <TableCell align="right">{roomInfo.name}</TableCell>
                    </TableRow>

                    <TableRow >
                      <TableCell component="th" scope="row">Тип комнаты</TableCell>
                      <TableCell align="right">{roomInfo.room_type}</TableCell>
                    </TableRow>
                    <TableRow >
                      <TableCell component="th" scope="row">Площадь</TableCell>
                      <TableCell align="right">{roomInfo.area} м<sup>2</sup></TableCell>
                    </TableRow>
                    <TableRow >
                      <TableCell component="th" scope="row">Можно купить</TableCell>
                      <TableCell align="right">{roomInfo.max_product}</TableCell>
                    </TableRow>
                    <TableRow >
                      <TableCell component="th" scope="row">Куплено</TableCell>
                      <TableCell align="right">{roomInfo.product_count}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={8}>
            {/* <Paper style={{background: "url(" + roomInfo.photo_url + ") 100% 100% no-repeat"}} className={classes.paper, classes.paperContainer}></Paper> */}
            <Paper style={{background: "url(https://avatars.mds.yandex.net/get-pdb/1662426/32e91c31-5f67-4437-b2d9-5c39257a828e/s1200) 100% 100% no-repeat"}} className={classes.paper, classes.paperContainer}></Paper>
          </Grid>
        </Grid>
      </Container >


      <TabMenu onChangeTabId={setCategoryId} />
      <Products categoryId={categoryId} />

    </div >
  );
}