import React, {useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import logo from '../../../assets/untitled.svg';
import axios from 'axios';
import config from '../../../../src/config.json'

const useStyles = makeStyles(theme => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
    },
}));

export default function Header({onChangeTabId}) {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);


    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        onChangeTabId(newValue)
        setValue(newValue)
    };

    console.log(value)

    
    const [balance, setData] = useState([]);

    useEffect(() => {
            axios.get(config.server_ip + '/users/1', {
                params: {
                }
            })
            .then(function (response) {  
                console.log(response.data)
                let arrResponse = [];
                arrResponse = response.data;
                console.log("**",arrResponse)
                setData(arrResponse);
            })
            .catch(function (error) {
                console.log(error);
            });
    },[]);
  

    return (
        <React.Fragment>
            <CssBaseline />
            <div position="fixed">
            <AppBar  color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <a href="/room"><img width="100" height="60" src={logo} alt="Logo" /></a>
                    <nav className={classes.toolbarTitle}>
                        <Link variant="button" variant="h5" color="textPrimary" href="/room" className={classes.link}>
                            Моя комната
                        </Link>
                        <Link variant="button" variant="h5" color="textPrimary" href="/catalog" className={classes.link}>
                            Каталог
                        </Link>
                    </nav>

                    {/* <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}> */}
                    <Button aria-controls="simple-menu" aria-haspopup="true" href="/pay">
                        <AccountBalanceWalletIcon style={{ fontSize: 30 }} />
                        <Typography variant="h6" align="left" component="h6">
                        ${balance.balance}
                        </Typography>
                    </Button>
                </Toolbar>
                <Paper>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                    >
                        <Tab label="Все категории" />
                        <Tab label="Мебель" />
                        <Tab label="Бытовая техника" />
                        <Tab label="Мультимедиа" />
                    </Tabs>
                </Paper>
            </AppBar>


    </div>
        </React.Fragment>
    );
}
