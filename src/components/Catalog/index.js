import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
//import Slideshow from '../Room';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ImgMediaCard from './CatalogItem';
import Box from '@material-ui/core/Box';
import jsonData from "./jsonData";
import jsonData2 from "./jsonData2";
import axios from 'axios';
import config from '../../config.json'
import { withStyles } from '@material-ui/core/styles';

import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';




const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    // maxWidth: 345,
    marginTop: '15vh',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return value === index && children
}

export default function FullWidthGrid({ categoryId }) {
  const classes = useStyles();

  const [products, setProducts] = useState([]);
  // let isFull = false;
  // let messageText = "Предмет успешно добавлен в комнату!"
  const [messageText, setText] = React.useState("Предмет успешно добавлен в комнату!");
  const [isFull, setFullness] = React.useState(false);

  useEffect(() => {

    axios.get(config.server_ip + '/rooms/1', {
      params: {
      }
    })
      .then(function (response) {

        console.log(response)
        if (response.data.max_product == response.data.product_count) {
          // isFull = true;
          setFullness(true)
          // messageText = "Вы не можете добавить больше предметов в комнату :("
          setText("Вы не можете добавить больше предметов в комнату :(")
        }

      })
      .catch(function (error) {
        console.log(error);
      });


    axios.get(config.server_ip + '/catalog', {
      params: {
      }
    })
      .then(function (response) {
        //console.log(response.data)
        let arrResponse = [];
        arrResponse = response.data.products;
        //arrResponse.push(response.data);
        console.log(arrResponse)
        setProducts(arrResponse);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);


  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };



  return (
    <div className={classes.root}>

      <Dialog onClose={handleClose} open={open}>
        <br /><br />
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {messageText}
        </DialogTitle>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Закрыть
          </Button>
        </DialogActions>
      </Dialog>



      <Container maxWidth="lg" component="main" className={classes.heroContent}>
        <Grid container spacing={3}>
          <TabPanel value={categoryId} index={0}>
            {
              products !== null && products.length !== 0 && products.map(({ price, id, name, photo_url }) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard isFull={isFull} showEror={handleClickOpen} photo_url={photo_url} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>

          <TabPanel value={categoryId} index={1}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 1).map(({ price, id, name, photo_url }) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard isFull={isFull} showEror={handleClickOpen} photo_url={photo_url} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>

          <TabPanel value={categoryId} index={2}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 2).map(({ price, id, name, photo_url }) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard isFull={isFull} showEror={handleClickOpen} photo_url={photo_url} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>

          <TabPanel value={categoryId} index={3}>
            {
              products !== null && products.length !== 0 && products.filter(product => product.category_id === 3).map(({ price, id, name, photo_url }) => {
                return (
                  <Grid item xs={6} sm={3}>
                    <ImgMediaCard isFull={isFull} showEror={handleClickOpen} photo_url={photo_url} id={id} name={name} key={id} price={price} />
                  </Grid>
                )
              })
            }
          </TabPanel>
        </Grid>
      </Container >
    </div>
  );
}