import React, {useState} from 'react'
import Header from '../components/Catalog/CatalogHeader'
import Container from '../components/Catalog'

export default function Catalog() {
    const [categoryId, setCategoryId] = useState(0);
    

    return (
        <div>
            <Header onChangeTabId={setCategoryId} />
            <Container categoryId={categoryId} />
        </div>
    )
}
