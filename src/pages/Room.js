import React from 'react'
import Body from '../components/Room/RoomBody'
import Header from '../components/RoomHeader'

export default function Contact() {

    return (
        <div>
            <Header />
            <Body />
        </div>
    )
}
