import React from 'react'
import Header from '../components/RoomTypesHeader'
import Body from '../components/RoomTypesBody'

export default function About() {
    return (
        <div>
            <Header />
            <Body />
        </div>
    )
}
