import React from 'react'
import Body from '../components/ProductPageBody'
import Header from '../components/RoomHeader'


export default function ProductPage() {
    return (
        <div>
            <Header />
            <Body />
        </div>
    )
}
