import React from 'react'
import Body from '../components/PayBody'
import Header from '../components/RoomTypesHeader'

export default function Pay() {
    return (
        <div>
            <Header />
            <Body />
        </div>
    )
}
