import React from 'react';
import Layout from './components/Layout'
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import RoomTypes from './pages/RoomTypes'
import Home from './pages/Home'
import Room from './pages/Room'
import Catalog from './pages/Catalog'
import Authorization from './pages/Authorization'
import Pay from './pages/Pay'
import ProductPage from './pages/ProductPage'
import Footer from './components/Footer'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
  root: {
    minHeight: '100vh',
  },
}));

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
      <Router>
        <div className={classes.root}>
          <Layout>
            <Switch>
              <Route path="/room">
                <Room />
              </Route>
              <Route path="/roomTypes">
                <RoomTypes />
              </Route>
              <Route path="/authorization">
                <Authorization />
              </Route>
              <Route path="/productPage">
                <ProductPage />
              </Route>
              <Route path="/catalog">
                <Catalog />
              </Route>
              <Route path="/pay">
                <Pay />
              </Route>
              <Route exact path="/">
                <Home />
              </Route>
            </Switch>
          </Layout>
        </div>
        <Footer />
      </Router>


    </ThemeProvider>
  );
}

export default App;
